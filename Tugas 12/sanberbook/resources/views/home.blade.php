@extends('base.master')
@section('title')
    Halaman Utama
@endsection
@section('sub-title')
    halaman utama
@endsection

@section('content')
<h1>SanberBook</h1>
<h2>Sosial Media Developer Santai Berkualitas</h2>

<p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>

<h3>Benefit Join di SanberBook</h3>

<ul>
    <li>Mendapatkan Motivasi dari sesama developer</li>
    <li>Sharing knowledge dari para mastah samber</li>
    <li>Dibuat oleh calon web developer terbaik</li>
</ul>

<h3>Cara Gabung ke SanberBook</h3>
<ol>
    <li>Mengunjungi Website ini</li>
    <li>Mendaftar di <a href="/register">Form Sign Up</a></li>
    <li>Selesai!</li>
</ol>
@endsection

