<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function selamatdatang(request $request){
        $name = $request['name'];
        return view('selamatdatang', ['name'=> $name]);
    }
}