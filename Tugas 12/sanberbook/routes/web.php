<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);
Route::get('/register', [AuthController::class, 'register']);
Route::post('/selamatdatang', [AuthController::class, 'selamatdatang']);

route::get('/table', function(){
    return view('page.table');
});

route::get('/data-table', function(){
    return view('page.data-table');
});

// create data
route::get('/cast/create', [CastController::class, 'create']);
route::post('/cast', [CastController::class,'tambah']); 

// read data
route::get('/cast', [CastController::class, 'index']);

// id tertentu
route::get('/cast/{id}', [CastController::class, 'show']);

// update data
route::get('/cast/{id}/edit', [CastController::class, 'edit']);

// update
route::put('/cast/{id}', [CastController::class, 'update']);

//delete data
route::delete('/cast/{id}', [CastController::class, 'destroy']);