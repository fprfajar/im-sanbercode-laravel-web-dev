@extends('base.master')
@section('title')
    Daftar Cast
@endsection
@section('sub-title')
    Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm my-2">Tambah Data</a>
<table class="table table-bordered">
    <thead>
      <tr>
        <th scope="col">Nomor</th>
        <th scope="col">Nama</th>
        <th scope="col">Aksi</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        @forelse ($cast as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>
                    
                    <form action="/cast/{{$item->id}}" method="POST">
                      @csrf
                      @method('delete')
                      <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                      <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    </form>

                </td>
            </tr>
        @empty
            <tr>
                <td>Data Kosong</td>
            </tr>
        @endforelse
      </tr>
        
    </tbody>
  </table>
@endsection