@extends('base.master')
@section('title')
    Isi data diri
@endsection
@section('sub-title')
    Buat Account
@endsection
@section('content')
<h1>Buat Account Baru!</h1>

<h3>Sign Up Form</h3>
<form action="/selamatdatang" method="post">
    @csrf
    <label>First Name:</label><br>
    <input type="text" name="name"><br><br>
    <label>Password:</label><br>
    <input type="password" name="pass"><br><br>
    <label>Gender:</label><br>
    <input type="radio" name="Gender">Male <br>
    <input type="radio" name="Gender">Female <br>
    <input type="radio" name="Gender">Other <br><br>
    <label>Nationality:</label><br>
    <select name="Nationality" id="">
        <option value="1">Indonesian</option>
        <option value="2">Singaporean</option>
        <option value="3">Malaysian</option>
        <option value="4">Other</option>
    </select><br><br>
    <label>Language Spoken:</label><br>
    <input type="checkbox" name="Language Spoken">Bahasa Indonesia <br>
    <input type="checkbox" name="Language Spoken">English <br>
    <input type="checkbox" name="Language Spoken">Other <br><br>
    <label>Bio:</label><br>
    <textarea name="message" id="" cols="30" rows="10"></textarea><br><br>


    <input type="submit" value="Sign up">

@endsection
