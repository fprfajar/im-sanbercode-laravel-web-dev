<?php
require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new animal("shaun");

echo "Name : ".$sheep->name."<br>"; // "shaun"
echo "Legs : ".$sheep->legs."<br>"; // 4
echo "Cold blooded : ".$sheep->cold_blooded."<br><br>"; // "no"

$frog = new frog("buduk");

echo "Name : ".$frog->name."<br>"; // "shaun"
echo "Legs : ".$frog->legs."<br>"; // 4
echo "Cold blooded : ".$frog->cold_blooded."<br>"; // "no"
echo "Jump : ".$frog->jump."<br><br>";

$ape = new ape("kera sakti");

echo "Name : ".$ape->name."<br>"; // "shaun"
echo "Legs : ".$ape->legs."<br>"; // 4
echo "Cold blooded : ".$ape->cold_blooded."<br>"; // "no"
echo "Jump : ".$ape->jump."<br><br>";
?>